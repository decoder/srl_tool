from flask import Blueprint
from flask import jsonify
from pysrlapi.main import spec

bp = Blueprint('home', __name__)


@bp.route('/')
def home():
    return jsonify(spec.to_dict())
