from flask import Blueprint, jsonify

errors = Blueprint('errors', __name__)

@errors.app_errorhandler(400)
def handle_bad_request_error(error):
    status_code = 400
    success = False
    response = {
        'success': success,
        'message': "Bad Request."
    }

    return jsonify(response), status_code


@errors.app_errorhandler(404)
def handle_not_found_error(error):
    status_code = 404
    success = False
    response = {
        'success': success,
        'message': "Not found."
    }

    return jsonify(response), status_code

@errors.app_errorhandler(405)
def handle_not_allowed_error(error):
    status_code = 405
    success = False
    response = {
        'success': success,
        'message': "The method is not allowed."
    }

    return jsonify(response), status_code

@errors.app_errorhandler(500)
def handle_internal_server_error(error):
    status_code = 500
    success = False
    response = {
        'success': success,
        'message': "Internal Server Error."
    }

    return jsonify(response), status_code


@errors.app_errorhandler(501)
def handle_not_implemented_error(error):
    status_code = 501
    success = False
    response = {
        'success': success,
        'message': 'Not Implemented Error. This fake service can only analyze a limited set of 1 sentence: "whichptr indicates which xbuffer holds the final iMCU row.".'
    }

    return jsonify(response), status_code


#@errors.app_errorhandler(Exception)
#def handle_unexpected_error(error):
    #status_code = 500
    #success = False
    #response = {
        #'success': success,
        #'message': 'An unexpected error has occurred.'
    #}

    #return jsonify(response), status_code

