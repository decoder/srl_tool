#!/usr/bin/env python3

from datetime import datetime as dt
import flask
from flask import (Blueprint, request, jsonify, make_response, abort)
import json
import jmespath
import logging
from marshmallow import Schema, fields, validate
import os
import sys
import time
import traceback
import urllib.parse

from .decoder_srl_tool_service import SRL

from pkm_python_client.pkm_client import PKMClient
from pkm_python_client.pkm_client import Log

api = Blueprint('api_v1', __name__)

PKM_USER = os.environ.get('PKM_USER')
PKM_PASSWORD = os.environ.get('PKM_PASSWORD')
PKM_CERT = os.environ.get('PKM_CERT')
if PKM_CERT is None:
    PKM_CERT = "/ssl/pkm_docker.crt"

debug = os.environ.get('DEBUG', default="true").lower() == "true"

lisa = SRL()


class PKMFileSRLSchema(Schema):
    # pkm project to query
    project_id = fields.Str(required=True)
    # pkm path to query to retrieve json data containing the text to analyze
    path = fields.Str(required=True)
    # json path allowing to retrieve the text to analyze in the retrieved data
    access = fields.Str(required=False)
    # the srl model to use. Currently unused.
    model = fields.Str(required=False)
    invocationID = fields.Str(required=False)


class PKMFileSRLResultSchema(Schema):
    message = fields.Str(required=True)
    status = fields.Boolean(required=True)


class Text(Schema):
    model = fields.Str(required=False)
    projectId = fields.Str(required=False)
    text = fields.Str(required=True)


class SRLChunk(Schema):
    text = fields.Str(required=True)
    pos = fields.Integer(required=True)
    len = fields.Integer(required=True)


class SRLPredicate(Schema):
    predicate = fields.Nested(SRLChunk, required=True)
    roles = fields.Dict(keys=fields.Str(validate=validate.OneOf(["ARG0",
                                                                 "ARG1",
                                                                 "ARG2",
                                                                 "ARG3",
                                                                 "ARG4",
                                                                 "ARG5",
                                                                 "ARGM-COM",
                                                                 "ARGM-LOC",
                                                                 "ARGM-DIR",
                                                                 "ARGM-GOL",
                                                                 "ARGM-MNR",
                                                                 "ARGM-TMP",
                                                                 "ARGM-EXT",
                                                                 "ARGM-REC",
                                                                 "ARGM-PRD",
                                                                 "ARGM-PRP",
                                                                 "ARGM-CAU",
                                                                 "ARGM-DIS",
                                                                 "ARGM-ADV",
                                                                 "ARGM-ADJ",
                                                                 "ARGM-MOD",
                                                                 "ARGM-NEG",
                                                                 "ARGM-DSP",
                                                                 "ARGM-LVB",
                                                                 "ARGM-CXN",
                                                                 "A0",
                                                                 "A1",
                                                                 "A2",
                                                                 "A3",
                                                                 "A4",
                                                                 "A5",
                                                                 "AA",
                                                                 "AM-ADV",
                                                                 "AM-CAU",
                                                                 "AM-DIR",
                                                                 "AM-DIS",
                                                                 "AM-EXT",
                                                                 "AM-LOC",
                                                                 "AM-MNR",
                                                                 "AM-MOD",
                                                                 "AM-NEG",
                                                                 "AM-PNC",
                                                                 "AM-PRD",
                                                                 "AM-TMP",
                                                                 "C-A0",
                                                                 "C-A1",
                                                                 "C-A2",
                                                                 "C-AM-MNR",
                                                                 "C-V",
                                                                 "LOC",
                                                                 "MISC",
                                                                 "ORG",
                                                                 "PER",
                                                                 "R-A0",
                                                                 "R-A1",
                                                                 "R-A2",
                                                                 "R-AM-CAU",
                                                                 "R-AM-LOC",
                                                                 "R-AM-MNR",
                                                                 "R-AM-TMP"])),
                        values=fields.Nested(SRLChunk),
                        required=True)


# ARG0
# ARG1
# ARG2
# ARG3
# ARG4
# ARG5
# ARGM-*:
# COM: Comitative
# LOC: Locative
# DIR: Directional
# GOL: Goal
# MNR: Manner
# TMP: Temporal
# EXT: Extent
# REC: Reciprocals
# PRD: Secondary Predication
# PRP: Purpose
# CAU: Cause
# DIS: Discourse
# ADV: Adverbials
# ADJ: Adjectival
# MOD: Modal
# NEG: Negation
# DSP: Direct Speech
# LVB: Light Verb
# CXN: Construction
# ## From CoNLL05
# A0
# A1
# A2
# A3
# A4
# A5
# AA
# AM-ADV
# AM-CAU
# AM-DIR
# AM-DIS
# AM-EXT
# AM-LOC
# AM-MNR
# AM-MOD
# AM-NEG
# AM-PNC
# AM-PRD
# AM-TMP
# C-A0
# C-A1
# C-A2
# C-AM-MNR
# C-V
# LOC
# MISC
# O
# ORG
# PER
# R-A0
# R-A1
# R-A2
# R-AM-CAU
# R-AM-LOC
# R-AM-MNR
# R-AM-TMP

class SRLResult(Schema):
    predicates = fields.List(fields.Nested(SRLPredicate),
                             required=True)
    text = fields.Str(required=False)

    # result = [
    # {
    # "predicate": ("indicates", 9, 9),
    # "ARG0": ("whichptr", 0, 8),
    # "ARG1": ("which xbuffer holds the final iMCU row", 19, 38)
    # },
    # {
    # "predicate": ("holds", 33, 5),
    # "ARG0": ("which xbuffer", 19 , 13),
    # "ARG1": ("the final iMCU row", 39, 18)
    # },
    # ]


@api.route('/srl', methods=['POST'])
def srl():
    """Do the Semantic Role Labeling of text
    ---
    post:
      parameters:
      - in: data
        schema: Text
      responses:
        200:
          schema: SRLResult
        400:
          description: Bad Request.
          content:
            application/json:
              schema: SRLResult
        500:
          description: An unexpected error has occurred.
          content:
            application/json:
              schema: SRLResult
    """

    app = flask.current_app
    text_schema = Text()
    form_data = request.get_json()
    # This below validates the JSON data existence and data type
    errors = text_schema.validate(form_data)
    if errors:
        return {
          "message": "Missing or sending incorrect data to analyze text."
            }, 400
    else:

        # model, currently unused, will be used to select models specialy
        # trained for a given use case
        if "model" in form_data:
            model = form_data['model']
        else:
            model = None
        if "projectId" in form_data:
            projectId = form_data['projectId']
        else:
            projectId = None
        text = form_data['text']

    logging.info(f"Analyzing {text}.")
    status, res = lisa.srl(text)

    if status:
        app.logger.info(f"srl SRL completed {status}, {res}.")
        logging.info(f"srl SRL completed {status}, {res}.")
        # res["message"] = "Analysis completed."
        # res["success"] = True
    else:
        app.logger.error(f"srl SRL internaly failed with {status}, {res}.")
        logging.error(f"srl SRL internaly failed with {status}, {res}.")
        #return {"predicates": []}, 500

    result_schema = SRLResult()

    # res = { "predicates" : [{'predicate': {"text":"indicates", "pos":9, "len":9},
    # 'roles': {'ARG0': {"text":"whichptr", "pos":0, "len":8},
    # 'ARG1': {"text":"which xbuffer holds the final iMCU row",
    # "pos":19, "len":38}} } ] }
    loaded = result_schema.load(res)

    logging.info(f"Returning {result_schema.dumps(loaded)}.")
    return result_schema.dumps(loaded), 200


def send_result(message, status, pkm=None, project_id=None, invocation_id=None,
                resultsToAdd=[]):
    assert type(resultsToAdd) == list, (f"resultsToAdd ({resultsToAdd}) should be a "
                                        f"list but it is a {type(resultsToAdd)}")
    app = flask.current_app
    if status < 300:
        logging.info(message)
        app.logger.debug(message)
    else:
        logging.error(message)
        app.logger.error(message)
    res = {
      "message": message,
      "status": (status < 300)}
    result_schema = PKMFileSRLResultSchema()
    loaded = result_schema.load(res)

    logging.info(f"Returning {result_schema.dumps(loaded)}.")
    if invocation_id is not None:
        # Put in invocationResults the paths of artifacts put in the pkm with type of
        # handled data ?
        pkm.update_invocation(
            project_id, invocation_id, 0 if status < 300 else status,
            resultsToAdd)
    return result_schema.dumps(loaded), status


@api.route('/pkmfilesrl', methods=['POST'])
def pkmfilesrl(invocationID=None):
    """ Semantic role labeling of the comments of a file stored in the PKM
    ---
    post:
      parameters:
      - name: key
        description: Access key to the PKM (optional, depending on server
                     configuration)
        in: header
        schema:
          type: string
      - name: invocationID
        in: path
        description: decoder invocation id
        required: false
        schema:
          type: string
      - in: data
        schema: PKMFileSRLSchema
      responses:
        200:
          description: Translation completed.
          content:
            application/json:
              schema: PKMFileSRLResultSchema
        500:
          description: An unexpected error has occurred.
          content:
            application/json:
              schema: PKMFileSRLResultSchema
    """
    try:
        app = flask.current_app

        key = None
        if 'keyParam' in request.headers:
            key = request.headers.get('keyParam')
        elif 'key' in request.headers:
            key = request.headers.get('key')

        form_data = request.get_json()
        project_id = None
        if "project_id" in form_data:
            project_id = form_data['project_id']
        invocation_id = request.args.get('invocationID', None)
        if invocation_id is None and 'invocationID' in form_data:
            invocation_id = form_data['invocationID']
        print(f"pkmfilesrl project_id: {project_id}, invocation_id: {invocation_id}",
            file=sys.stderr)

        pkm = PKMClient(cacert=PKM_CERT, key=key)
        log = Log(pkm=pkm, project=project_id, tool="srl_tool")

        if key is None and PKM_USER and PKM_PASSWORD:
            if debug:
                flask.current_app.logger.debug("user's login")
            status = pkm.login(user_name=PKM_USER, user_password=PKM_PASSWORD)
            if not status:
                message = f"user's login to pkm failed: {status}"
                flask.current_app.logger.error(message)
                return send_result(message, 500, pkm, project_id, invocation_id)
        elif key is None:
            errorString = (
            "Either pkm key should be passed as http header 'key' or"
            " user/password env vars should be defined")
            flask.current_app.logger.error(errorString)
            return send_result(errorString, 400, pkm, project_id, invocation_id)

        srl_schema = PKMFileSRLSchema()
        errors = srl_schema.validate(form_data)

        if errors:
            message = (f"Missing or sending incorrect data. Could not validate "
                    f"{form_data} with respect to {srl_schema}."
                    f"Error: {errors}.")
            log.error(message)
            app.logger.error(message)
            return {"message": message, "success": False}, 400
        # pkm project to query
        # pkm path to query to retrieve json data containing the text to analyze
        path = urllib.parse.unquote(form_data['path'])
        # access: json path allowing to retrieve the text to analyze in the
        # retrieved data
        if "access" in form_data:
            access = form_data['access']
        else:
            access = None

        # model, currently unused, will be used to select models specialy
        # trained for a given use case
        if "model" in form_data:
            model = form_data['model']
        else:
            model = None

        if debug:
            message = (f"pkmfilesrl project_id: {project_id}, path: {path}, "
                    f"access: {access}, model: {model}")
            app.logger.debug(message)
            log.message(message)

        if debug:
            message = f"getting {path}"
            app.logger.debug(message)
            log.message(message)
        status, doc = pkm.call(method="GET", path=path)
        if status != 0:
            return send_result("Getting comments failed.", 500, pkm, project_id,
                            invocation_id)

        if debug:
            message = f"retrieved doc is {json.dumps(doc)}"
            app.logger.debug(message)
            log.message(message)
        texts = []
        if access is None:
            if not isinstance(doc, list):
                texts = [doc]
            else:
                texts = doc
        else:
            access = urllib.parse.unquote(access)
            if debug:
                message = f"json path is: {access}"
                app.logger.debug(message)
                log.message(message)
            try:
                texts = jmespath.search(access, doc)
            except jmespath.exceptions.EmptyExpressionError as e:
                message = f"JMESpath search failed on empty access {access}: {e}"
                app.logger.debug(message)
                log.message(message)
                return send_result(message, 400, pkm, project_id, invocation_id)
            except Exception as e:
                message = f"JMESpath search failed on {access}: {e}"
                app.logger.debug(message)
                log.message(message)
                return send_result(message, 400, pkm, project_id, invocation_id)
            if texts is None:
                texts = []
            if debug:
                message = f"selected texts are: {texts}"
                app.logger.debug(message)
                log.message(message)

        stexts = "\n".join(texts)
        stexts = stexts.replace("*", " ")
        texts = [stexts]

        results = []
        for text in texts:
            message = f"Analyzing {text}."
            logging.info(message)
            log.message(message)
            status, res = lisa.srl(text, return_text=True)

            if status:
                message = f"pkmfilesrl SRL completed {status}, {res}."
                app.logger.info(message)
                log.message(message)
                results.append(res)
            else:
                message = f"pkmfilesrl SRL completed {status}, {res}. Continuing on remaining texts if any."
                app.logger.error(message)
                log.error(message)
                results.append("")

        result = {}
        result['path'] = path
        result['access'] = access
        result['srl'] = results

        # TODO implement writing the srl at the right location in the annotations.
        if debug:
            message = (f"writing back srl result of project {project_id} "
                    f"and path {urllib.parse.quote(path, safe='')}")
            app.logger.debug(message)
            log.message(message)
            message = f"result to write is: {result}"
            app.logger.debug(message)
            log.message(message)
        status, res = pkm.call(
            method="PUT",
            path=f"annotations/{project_id}",
            payload=[result])
        if status != 0:
            message = (f"Writing back srl result of project {project_id} "
                    f"and path {path} failed")
            app.logger.error(message)
            log.error(message)
            return send_result(message, 500, pkm, project_id, invocation_id)

        return send_result("Success", 200, pkm, project_id, invocation_id,
                        [dict(path=(f"/annotations/{project_id}"
                                    f"?path={urllib.parse.quote(path, safe='')}"
                                    f"&access={urllib.parse.quote(access, safe='')}"),
                                type="annotations")])
    except Exception as e:
        message = (f"Got an unexpected exception in pkmfilesrl: {e}. Stackstrace: \n{traceback.format_exc()}")
        app.logger.error(message)
        log.error(message)
        return send_result(message, 500, pkm=None, project_id=None, invocation_id=None)


@api.after_request
def after_request(response):
    """ Logging after every request. """
    app = flask.current_app

    # logger = logging.getLogger("app.access")
    if debug:
        app.logger.debug(
            "%s [%s] %s %s %s %s %s %s %s",
            request.remote_addr,
            dt.utcnow().strftime("%d/%b/%Y:%H:%M:%S.%f")[:-3],
            request.method,
            request.path,
            request.scheme,
            response.status,
            response.content_length,
            request.referrer,
            request.user_agent,
        )
    return response
