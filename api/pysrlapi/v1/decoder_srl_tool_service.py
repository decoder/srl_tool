#!/usr/bin/env python3

from inspect import currentframe, getframeinfo
import srl
import sys
import traceback

class SRL:

    def __init__(self):
        """"""

        # self._analyzer = srl.Analyzer(
        # {"save_dir": "/home/gael/Projets/Decoder-ICT-16/srl/LISA-v1/model"})
        self._analyzer = srl.Analyzer({"save_dir": "/LISA-v1/model"})

    def srl(self, text: str, return_text: bool = False):
        '''
        Do semantic role labeling of given text.
        '''

        print(f"SRL.srl '{text[:50]}'", file=sys.stderr)
        all_predicates = []
        try:
            conll = self._analyzer.analyze(text)
            print(f"SRL.srl conll output:\n{conll}", file=sys.stderr)
            str_sents = [sent.strip() for sent in conll.split('\n\n')]
            sents = []
            # Remove last sent if empty
            if str_sents and not str_sents[-1]:
                str_sents.pop()
            for sent_id, sent in enumerate(str_sents):
                print(f"SRL.srl handling sentence n°{sent_id}: '{sent}'",
                      file=sys.stderr)
                if not sent:
                    continue
                this_sent_tokens = {}  # k = tok_id, v = token data tuple
                # the number of predicates in the sentence = the length of the fields
                # element in token tuples
                nb_predicates_in_sent = 0
                str_toks = [tok.strip() for tok in sent.split('\n')]
                for tok_id, str_tok in enumerate(str_toks):
                    print(f"SRL.srl {tok_id} : {str_tok}", file=sys.stderr)
                    fields = str_tok.split("\t")
                    print(f"SRL.srl {tok_id} fields: {fields}", file=sys.stderr)
                    if len(fields) < 13:
                        print(f"SRL.srl Wrong token line n°{tok_id} in sentence "
                              f"n°{sent_id}. No enough fields in: '{fields}'",
                              file=sys.stderr)
                        continue
                    # conll05 combined format
                    # Field 1: domain placeholder
                    # Field 2: sentence id
                    # Field 3: token id
                    # Field 4: word form
                    # Field 5: gold part-of-speech tag
                    # Field 6: auto part-of-speech tag
                    # Field 7: dependency parse head
                    # Field 8: dependency parse label
                    # Field 9: placeholder
                    # Field 10: verb sense
                    # Field 11: predicate (infinitive form)
                    # Field 12: position (placeholder in Conll05)
                    # Field 13: length (placeholder in Conll05)
                    # Field 14: NER placeholder
                    # Field 15+: for each predicate, a column representing the labeled arguments of the predicate.

                    # domain	sid	tid	token      	pos	pos	hea	dep	    ph	vs	predicate	ph	ph	ner	field+
                    # conll05	0	17	Revitalized	VBN	VBN	19	amod	_	01	revitalize	-	-	O	B-V	B-A0	O	I-AM-LOC
                    (_, _, tok_id, tok, _, tag, dep_head, dep_label, _, _, pred_label, pos, length) = fields[:13]
                    fields = fields[14:]
                    nb_predicates_in_sent = len(fields)
                    this_sent_tokens[tok_id] = (pos, length, tok, tag, dep_head,
                                                dep_label, pred_label, fields)
                    # fields = array of length the nuber of predicates in the
                    # sentence. At each position n, the role of the current token
                    # for predicate n°n.
                    print(f"SRL.srl {tok} : {str_tok}, {fields}", file=sys.stderr)
                print(f"SRL.srl there is {nb_predicates_in_sent} predicates in "
                      f"sentence {sent_id}", file=sys.stderr)
                this_sent_predicates = [
                  {"predicate": {"text": '', "pos": pos, "len": length},
                   "roles": {}} for _ in range(nb_predicates_in_sent)]
                # go through the list of fields
                for pred_id in range(nb_predicates_in_sent):
                    print(f"SRL.srl handling predicate n°{pred_id}", file=sys.stderr)
                    arg_pos = None
                    arg_typ = None
                    arg_len = None
                    arg_str = None
                    # go through this sentence tokens a first time to fill the
                    # predicate field
                    for tok_id, (pos, length, tok, tag, dep_head, dep_label,
                                 pred_label, fields) in this_sent_tokens.items():
                        field = fields[pred_id]
                        print(f"SRL.srl handling predicate n°{pred_id}, tok {tok_id}: "
                              f"{tok}, fields: {fields}, field: {field}",
                              file=sys.stderr)
                        if field == 'B-V':
                            print(f"SRL.srl set predicate {pred_id} to be ({tok}, "
                                  f"{pos}, {length}) ",
                                  file=sys.stderr)
                            this_sent_predicates[pred_id]["predicate"] = {
                              "text": tok, "pos": pos, "len": length}
                            # then go through this sentence tokens a second time to
                            # fill the args of the predicate
                            arg_pos = 0
                            arg_typ = ''
                            arg_len = 0
                            arg_str = ''
                        else:
                            # non-arg token: finishes the current arg if any
                            if field == 'O':
                                print("non-arg token: finishes the current arg if any")
                                # there is a current arg, insert it in result
                                if arg_typ:
                                    print(f"SRL.srl non role token. add "
                                          f"previously building one: ({arg_typ}, "
                                          f"{arg_str}, {arg_pos}, {arg_len}) "
                                          f"to predicate {pred_id}",
                                          file=sys.stderr)
                                    this_sent_predicates[pred_id]["roles"][arg_typ] = {
                                      "text": arg_str, "pos": arg_pos, "len": arg_len}
                                # we are no more building an arg
                                arg_pos = 0
                                arg_typ = ''
                                arg_len = 0
                                arg_str = ''
                            # arg token: start a new arg from none,
                            # or start a new one finishing the current one
                            # or continues the current one
                            else:
                                print("arg token")
                                field_parts = field.split('-')
                                field_type = field_parts[0]
                                new_arg_type = '-'.join(field_parts[1:])
                                # start a new arg: from none or finishing the current
                                # one
                                if field_type == 'B':
                                    # there is a current one, finalize it
                                    if arg_typ:
                                        print(f"SRL.srl starting new role. add "
                                              f"previous one: ({arg_typ}, "
                                              f"{arg_str}, {arg_pos}, {arg_len}) "
                                              f"to predicate {pred_id}",
                                              file=sys.stderr)
                                        this_sent_predicates[pred_id]["roles"][arg_typ] = {
                                          "text": arg_str,
                                          "pos": arg_pos,
                                          "len": arg_len}
                                    # initialize the new arg
                                    if new_arg_type == 'V':
                                        # this is not an arg
                                        print("new_arg_type: 'V' this is not an arg")
                                        arg_pos = 0
                                        arg_typ = ''
                                        arg_len = 0
                                        arg_str = ''
                                    else:
                                        arg_pos = int(pos)
                                        arg_typ = new_arg_type
                                        arg_len = int(length)
                                        arg_str = tok
                                        print(f"new_arg_type: '{new_arg_type}' ({arg_typ}, "
                                              f"{arg_str}, {arg_pos}, {arg_len})")
                                # continues current arg
                                elif field_type == 'I':
                                    # arg_pos = no change
                                    # arg_typ = no change
                                    arg_len += (int(pos) + int(length)) - arg_pos
                                    arg_str += f" {tok}"
                    if arg_typ:
                        print(f"SRL.srl role after last token: ({arg_typ}, "
                              f"{arg_str}, {arg_pos}, {arg_len}) "
                              f"to predicate {pred_id}",
                              file=sys.stderr)
                        this_sent_predicates[pred_id]["roles"][arg_typ] = {
                          "text": arg_str,
                          "pos": arg_pos,
                          "len": arg_len}
                # We found the information of the current pred_id
                for predicate in this_sent_predicates:
                    if predicate["predicate"]["text"]:
                        print(f"SRL.srl adding predicate to all_predicates: "
                              f"{predicate}", file=sys.stderr)
                        all_predicates.append(predicate)
                    else:
                        print(f"SRL.srl no text in: {predicate}", file=sys.stderr)
        except IndexError as e:
            frameinfo = getframeinfo(currentframe())
            print(f"Catching an IndexError exception at: {frameinfo}",
                  file=sys.stderr)
            # printing stack trace
            traceback.print_exc()
            return False, {"predicates": []}

        print(f"SRL.srl all_predicates: {all_predicates}", file=sys.stderr)
        result = {"predicates": all_predicates}
        if return_text:
            result["text"] = text

        return True, result
