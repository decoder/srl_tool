#!/usr/bin/env python3

import os
from logging.config import dictConfig
import json
from flask import Flask
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields
import ca_bundle

ca_bundle.install()



from .v1.routes import api as api_v1

# import endpoints from routes. Make them available in test_request_context below
from .v1.routes import srl as srl
from .v1.routes import pkmfilesrl as pkmfilesrl

spec = APISpec(
    title="Decoder SRL tool",
    version="0.0.1",
    openapi_version="3.0.2",
    info=dict(description="A Semantic Role Labeling tool for the Decoder project"),
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)


def _initialize_errorhandlers(application):
    '''
    Initialize error handlers
    '''
    from .errors import errors
    application.register_blueprint(errors)


def create_app():

    dictConfig({
        'version': 1,
        'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }},
        'handlers': {'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }},
        'root': {
            'level': 'DEBUG',
            'handlers': ['wsgi']
        }
    })

    # Create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config['SECRET_KEY'] = 'b0bff1b3-32d1-4ec5-b6e6-f31af9ed682e'

    with app.app_context():
        #ensure_dir(app.instance_path)  # Ensure the instance folder exists

        ## Create temp directory where text files will be stored
        #app.config.txt_folder = os.path.join(app.instance_path, "temp_text")
        #ensure_dir(app.config.txt_folder)

        # Register home blueprint
        from . import home
        app.register_blueprint(home.bp)
        _initialize_errorhandlers(app)

        # Register api v1
        from .v1.routes import api as api_v1
        app.register_blueprint(api_v1, url_prefix="/")

        return app


app = create_app()

# we need to be in a Flask request context
with app.test_request_context():
    spec.path(view=pkmfilesrl)
    spec.path(view=srl)
