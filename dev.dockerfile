FROM tiangolo/uwsgi-nginx-flask:python3.6

# --------------------
# Package installation
# --------------------

ARG DEBIAN_FRONTEND=noninteractive
ARG THREADS

RUN apt-get clean && apt-get update && apt-get install -y -qq \
  apt-utils \
  bzip2 \
  build-essential \
  curl \
  dialog \
  gawk \
  locales \
  openssl \
  python3 \
  python3-pip \
  wget

COPY requirements.txt /
RUN pip install --upgrade pip
RUN pip install -r /requirements.txt

ARG PUID
ARG GUID

# CREATE APP USER
RUN groupadd -g ${GUID} apiuser && \
  useradd -r -u ${PUID} -g apiuser apiuser

# ARG CACHEBUST=1
# RUN echo "$CACHEBUST"

WORKDIR /
RUN git clone https://github.com/kleag/LISA-v1.git LISA-v1
WORKDIR /LISA-v1
RUN pip install -r /LISA-v1/requirements.txt
RUN ls
# COPY models/LISA-v1-model-59.35.tgz .
# RUN tar xvzf LISA-v1-model-59.35.tgz
COPY models/LISA-v1-model-decoder-60.44-20211112.tgz .
RUN tar xvzf LISA-v1-model-decoder-60.44-20211112.tgz
RUN mv model-decoder model
WORKDIR /LISA-v1/model
COPY models/glove.6B.100d.txt.tgz .
RUN tar xvzf glove.6B.100d.txt.tgz
COPY models/transition_probs.tsv .
# COPY models/config.cfg .
COPY models/config-model-decoder-60.44-20211112.cfg ./config.cfg
WORKDIR /LISA-v1
RUN python -m spacy download en_core_web_sm
RUN python srl.py --save_dir model README.md
RUN cat README.md.srl.conll

COPY ./api /app
COPY ./ssl /ssl
RUN echo "gid = ${GUID}" >> /app/uwsgi.ini
RUN echo "uid = ${PUID}" >> /app/uwsgi.ini
RUN chown -R ${PUID}:${GUID} /app

# install SSL certificate of PKM server
RUN mkdir -p /usr/local/share/ca-certificates && \
  chmod 755 /usr/local/share/ca-certificates && \
  cp -f /ssl/pkm_docker.crt /usr/local/share/ca-certificates && \
  chmod 644 /usr/local/share/ca-certificates/pkm_docker.crt && \
  update-ca-certificates

#RUN git pull origin master
