#!/bin/bash

docker-compose --env-file ./srl.env build --build-arg CACHEBUST=$(date +%s)
