#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

API=$(cd $(dirname $0); pwd)
COMPRESSED=yes
TRACE=yes
PKM_PROTOCOL="http"
PKM_HOST="pkm-api_pkm_1"
PKM_PORT="8080"
FRAMAC_HOST="pkm-api_frama-c_1"
FRAMAC_PORT="8081"
CERT="pkm_docker.crt"

tracefile=$0
tracefile="${tracefile%.*}.trace"

function exit_status
{
    local STATUS=$1
    if [ $STATUS -eq 0 ]; then
        echo "Test PASSED" >&2
    else
        echo "Test FAILED" >&2
    fi

    exit $STATUS
}

function escape_file_content
{
    IFS= read -r -d '' content < $1
    content=${content//'\'/'\\'}
    content=${content//$'\t'/'\t'}
    content=${content//'"'/'\"'}
    content=${content//$'\n'/'\n'}
    echo "$content"
}

function json_one_liner
{
    IFS= read -r -d '' content < $1
    content=${content//$'\t'/' '}
    content=${content//$'\n'/}
    echo "$content"
}

function curl
{
    local CURL=$(which curl)
    local TRACE_FILE
    local OUT_FILE
    local HEADERS_FILE
    local args=("$@")
    local i=0
    local CURL_CMD=("$CURL")
    if [ "${COMPRESSED}" = "yes" ]; then
        CURL_CMD+=('--compressed')
    fi
    if [ "${TRACE}" = "yes" ]; then
        TRACE_FILE=$(mktemp curl.XXX)
        CURL_CMD+=('--trace-ascii' "${TRACE_FILE}")
    fi
    CURL_CMD+=('-s' '-S')
    OUT_FILE=$(mktemp curl.XXX)
    CURL_CMD+=('-o' "${OUT_FILE}")
    HEADERS_FILE=$(mktemp curl.XXX)
    CURL_CMD+=('-D' "${HEADERS_FILE}")
    while [ $i -lt $# ]; do
        CURL_CMD+=("${args[$i]}")
        i=$(($i+1))
    done
    "${CURL_CMD[@]}"
    if [ $? -ne 0 ]; then
        echo >&2
        echo "call to pkm failed" >&2
        exit_status 1
    fi
    http_code=$(grep "HTTP/1.1" ${HEADERS_FILE} | tail -n 1 | sed -e "s/HTTP\/1.1 //" -e "s/ .*//")
    local STATUS=0
    if [ ${http_code} -lt 300 ] ;
    then
        STATUS=0 ;
        cat ${OUT_FILE}
    else
        echo "Call to PKM server failed:" >&2
        printf "'%s' " "${CURL_CMD[@]}" >&2
        echo >&2
        cat ${HEADERS_FILE} >&2
        cat ${OUT_FILE} >&2
        STATUS=${http_code} ;
    fi
    if [ "${TRACE}" = "yes" ]; then
        printf "'%s' " "${CURL_CMD[@]}" >> ${tracefile}
        cat "${TRACE_FILE}" >> ${tracefile}
        rm -f "${TRACE_FILE}"
        cat "${HEADERS_FILE}" >> ${tracefile}
        cat "${OUT_FILE}" >> ${tracefile}
    fi

    rm -f "${HEADERS_FILE}"
    rm -f "${OUT_FILE}"
    echo "curl returns ${STATUS}" >&2
    return "${STATUS}"
}

function login
{
  if [ "$#" -ne 2 ]; then
      echo "Error in login function: takes 2 arguments: user name and password" >&2
      exit_status 1
  fi
  USER_NAME=$1
  PASSWORD=$2
  echo "user's login" >&2
  USER1_KEY=$(curl --cacert "${API}/ssl/${CERT}" -X POST "${PKM_PROTOCOL}://${PKM_HOST}:${PKM_PORT}/user/login" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"user_name\":\"${USER_NAME}\",\"user_password\":\"${PASSWORD}\"}" | jq .key | sed -n 's/^"\([^"]*\)"$/\1/p')
  if [ $? -ne 0 ]; then
      echo >&2
      echo "user's login failed" >&2
      exit_status 1
  fi
  echo "${USER1_KEY}"
  return 0
}

function project
{
  if [ "$#" -ne 2 ]; then
      echo "Error in project function: takes 2 arguments: access key and project name"
      exit_status 1
  fi
  ACCESS_KEY=$1
  PROJECT_NAME=$2
  echo "getting project ${PROJECT_NAME}" >&2
  PROJECT=$(curl --cacert "${API}/ssl/${CERT}" -X GET "${PKM_PROTOCOL}://${PKM_HOST}:${PKM_PORT}/project/${PROJECT_NAME}" -H  "accept: application/json" -H  "key: ${ACCESS_KEY}")
  if [ $? -ne 0 ]; then
      echo >&2
      echo "getting project ${PROJECT_NAME} failed" >&2
      exit_status 1
  fi
  echo "${PROJECT}"
  return 0
}

function comments_get
{
  if [ "$#" -ne 4 ]; then
      echo "Error in comments_get function: takes 4 arguments: access key, project name, language name and (url encoded) file name"
      exit_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local LANGUAGE=$3
  local FILE_NAME=$4
  echo "getting comments for ${FILE_NAME} in project ${PROJECT_NAME}" >&2
  COMMENTS=$(curl --cacert "${API}/ssl/${CERT}" -X GET "${PKM_PROTOCOL}://${PKM_HOST}:${PKM_PORT}/code/${LANGUAGE}/comments/${PROJECT_NAME}/${FILE_NAME}?limit=0" -H  "accept: application/json" -H  "key: ${ACCESS_KEY}")
  if [ $? -ne 0 ]; then
      echo >&2
      echo "getting comments ${FILE_NAME} in ${PROJECT_NAME} failed" >&2
      exit_status 1
  fi
  echo "${COMMENTS}"
  return 0
}


function annotations_get
{
  if [ "$#" -ne 3 ]; then
      echo "Error in annotations_get function: takes 3 arguments: access key, project name and (url encoded) path"
      exit_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local PKM_PATH=$3
  echo "getting annotations for ${PKM_PATH} in project ${PROJECT_NAME}" >&2
  ANNOTATIONS=$(curl --cacert "${API}/ssl/${CERT}" -X GET "${PKM_PROTOCOL}://${PKM_HOST}:${PKM_PORT}/annotations/${PROJECT_NAME}/${PKM_PATH}" -H  "accept: application/json" -H  "key: ${ACCESS_KEY}")
  if [ $? -ne 0 ]; then
      echo >&2
      echo "getting annotations ${PKM_PATH} in ${PROJECT_NAME} failed" >&2
      exit_status 1
  fi
  echo "${ANNOTATIONS}"
  return 0
}

function annotations_post
{
  if [ "$#" -ne 4 ]; then
      echo "Error in annotations_post function: takes 4 arguments: access key, project name, (url encoded) path and annotations"
      exit_status 1
  fi
  local ACCESS_KEY=$1
  local PROJECT_NAME=$2
  local PKM_PATH=$3
  local ANNOTATIONS=$4
  echo "posting annotations ${ANNOTATIONS} for ${PKM_PATH} in project ${PROJECT_NAME}" >&2
  curl --cacert "${API}/ssl/${CERT}" -X POST "${PKM_PROTOCOL}://${PKM_HOST}:${PKM_PORT}/annotations/${PROJECT_NAME}/${PKM_PATH}" -H  "Content-Type: application/json" -H "accept: application/json" -H  "key: ${ACCESS_KEY}" -d "[${ANNOTATIONS}]"
  if [ $? -ne 0 ]; then
      echo >&2
      echo "posting annotations ${ANNOTATIONS} to ${PKM_PATH} in ${PROJECT_NAME} failed" >&2
      exit_status 1
  fi
  return 0
}
