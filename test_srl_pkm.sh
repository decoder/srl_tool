#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/pkm_rest_api.sh"

PKM_USER=garfield
PKM_PASSWORD=hello
host="localhost"
port=701

PROJECT_NAME="mydb"



function encode
{
  result=$(python -c "import urllib.parse;print(urllib.parse.quote('$1'))")
  echo $result
}

PKM_PATH=$(encode "code/c/comments/mydb" )
# [{"global_kind":"GCompTag","loc":{"pos_start":{"pos_path":"examples/vector2.c","pos_lnum":5,"pos_bol":107190,"pos_cnum":107190},"pos_end":{"pos_path":"examples/vector2.c","pos_lnum":5,"pos_bol":107190,"pos_cnum":107196}},"comments":["#include \"stdio.h\"\\n"]},{"global_kind":"GFun","loc":{"pos_start":{"pos_path":"examples/vector2.c","pos_lnum":20,"pos_bol":107483,"pos_cnum":107488},"pos_end":{"pos_path":"examples/vector2.c","pos_lnum":25,"pos_bol":107640,"pos_cnum":107641}},"comments":[" Sum of two vectors\\n"]},{"global_kind":"GFun","loc":{"pos_start":{"pos_path":"examples/vector2.c","pos_lnum":43,"pos_bol":108059,"pos_cnum":108063},"pos_end":{"pos_path":"examples/vector2.c","pos_lnum":46,"pos_bol":108182,"pos_cnum":108183}},"comments":[" Scalar product of two vectors\\n"]},{"stmt_id":27,"loc":{"pos_start":{"pos_path":"examples/vector2.c","pos_lnum":70,"pos_bol":108604,"pos_cnum":108605},"pos_end":{"pos_path":"examples/vector2.c","pos_lnum":70,"pos_bol":108604,"pos_cnum":108614}},"comments":["\tfprintf(stdout,\"%f\\n\",norm(c));\\n"]}]

# $[*]["comments"][*]
# $[?(global_kind == "GFun")].comments
# ACCESS=$(encode "[?global_kind == \'GFun\'] | [?loc.pos_start.pos_path == \'examples/vector2.c\'].comments | [0]")
ACCESS=$(encode "[*].comments[?global_kind == \'GFun\'][] | [?loc.pos_start.pos_path == \'examples/vector2.c\'].comments[]")

USER1_KEY=$(login ${PKM_USER} ${PKM_PASSWORD})
echo "Login was successful"

command="curl --request POST  --url http://${host}:${port}/pkmfilesrl --header 'content-type: application/json' --header 'accept: application/json' --header 'key: ${USER1_KEY}' --data '{\"project_id\": \"${PROJECT_NAME}\", \"path\": \"${PKM_PATH}\", \"access\": \"${ACCESS}\"}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
