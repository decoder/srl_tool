#!/bin/bash

host="localhost"
port=701
# text="whichptr indicates which xbuffer holds the final iMCU row."
text="Returns a Booking by its id 'id'. @param id The id 'id' of the Booking. @return The {@link BookingEto} with id 'id'"

command="curl --request POST  --url http://$host:$port/srl --header 'content-type: application/json'   --data '{\"text\": \"$text\", \"projectId\": \"test_project\"}'"

cat <<EOF
command:
$command

response:
$(eval ${command})
EOF
