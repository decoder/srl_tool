#!/usr/bin/env python3

from api.pysrlapi.main import create_app
from api.pysrlapi.main import spec
from api.pysrlapi.v1.routes import srl
from api.pysrlapi.v1.routes import SRLResult
from flask import Flask

import json
from pprint import pprint

app = create_app()

# we need to be in a Flask request context
with app.test_request_context():
    spec.path(view=srl)
    print(f'Generated spec is:\n{json.dumps(spec.to_dict(), sort_keys=True, indent=2)}')
