#!/usr/bin/env python3

import json
from flask import Flask
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields

spec = APISpec(
    title="Gisty",
    version="1.0.0",
    openapi_version="3.0.2",
    info=dict(description="A minimal gist API"),
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)


app = Flask(__name__)


class GistParameter(Schema):
    gist_id = fields.Int()


class GistSchema(Schema):
    id = fields.Int()
    content = fields.Str()


@app.route("/gists/<gist_id>")
def gist_detail(gist_id):
    """Gist detail view.
    ---
    get:
        parameters:
            - in: path
              schema: GistParameter
        responses:
            200:
                schema: GistSchema
   """
    return "details about gist {}".format(gist_id)


# Since `path` inspects the view and its route,
# we need to be in a Flask request context
with app.test_request_context():
    spec.path(view=gist_detail)

print(f'Generated spec is:\n{json.dumps(spec.to_dict(), sort_keys=True, indent=2)}')
